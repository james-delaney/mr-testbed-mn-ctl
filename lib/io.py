
from machine import Pin, I2C
from ssd1306 import SSD1306_I2C

i2c_m = None
ssd1306 = None
def init_io():
    global i2c_m

    i2c_m = I2C(0, I2C.MASTER, baudrate=100000, pins=("P9", "P10"))

    init_display()


def init_display():
    global i2c_m, ssd1306

    ssd1306 = SSD1306_I2C(128, 64, i2c_m)

    ssd1306.text("MR Client", 0, 0)
    ssd1306.show()

def update_display(wifi_status, lora_status, active_radio):
    
    wifi_link = wifi_status[0]
    wifi_rssi = wifi_status[1]
    lora_link = lora_status[0]
    lora_rssi = lora_status[1]

    ssd1306.fill(0)

    if wifi_link:
        ssd1306.text('WiFi: %d' % (wifi_rssi), 0, 17)
    else:
        ssd1306.text('WiFi: DN', 0, 16)

    if lora_link:
        ssd1306.text('LoRa: %d' % (lora_rssi), 0, 25)
    else:
        ssd1306.text('LoRa: DN', 0, 25)

    if active_radio == 0:
        ssd1306.text('Sel: LoRa', 0, 33)
    elif active_radio == 1:
        ssd1306.text('Sel: WiFi', 0, 33)
    else:
        ssd1306.text('Sel: None', 0, 33)

    ssd1306.show()

