import ulab as np

class RLRadio():

    def __init__(self, radio_id, radio_str,freq, bw, rx_sensitivity, bit_rate, tx_power_steps, pwr_steps, tx_pow_set_cb, tx_pow_map):
        '''
            freq = frequency in MHz
            bw = bandwidth in MHz
            rx_sensitivity = receiver sensitivity in dBm
            tx_power_steps = list of possible transmit powers (in dBm)
            pwr_steps = list of power consumptions (in mA) at each tx power step (must be same size)
        '''
        self.radio_id = radio_id
        self.freq = freq
        self.bw = bw
        self.rx_sensitivity = rx_sensitivity
        self.bit_rate = bit_rate
        self.tx_power_steps = tx_power_steps
        self.pwr_steps = pwr_steps
        self.curr_tx_pow = 0
        self.snr = 0
        self.radio_str = radio_str

        self.RSSI_to_dst = 0                   # rssi in dBm
        self.fade_margin_to_dst = 0            # fade margin in dB
        self.link_status = 0

        self.pwr_per_bit = []
        self.bits_per_ma = []
        self.pow_W = []
        self.norm_pwr_steps = []

        for tx_power in self.tx_power_steps:
            self.pwr_per_bit.append(tx_power/self.bit_rate)

        for tx_power in pwr_steps:
            self.bits_per_ma.append(self.bit_rate/tx_power)
            self.pow_W.append((tx_power * 0.001) * 3.3) # power calculation assuming 3.3V transceiver

        '''
        self.noise_floor = (10*np.log10(k*T*bw*10**6))+30
        self.noise = -174
        '''
        self.set_norm_pwr_steps(np.linspace(0,1,10))
        self.norm_bit_rate = 0

        self.tx_pow_set_cb = tx_pow_set_cb
        self.tx_pow_map = tx_pow_map

    def get_pwr_ber_bit(self):
        return self.pwr_per_bit[self.curr_tx_pow]

    def get_bits_per_ma(self):
        return self.bits_per_ma

    def get_radio_id(self):
        return self.radio_id

    def get_freq(self):
        return self.freq

    def get_rx_sensitivity(self):
        return self.rx_sensitivity

    def get_pow_W(self):
        return self.pow_W[self.curr_tx_pow]

    def get_pow_W_at_step(self, tx_pow_step):
        return self.pow_W[tx_pow_step]

    def get_tx_pow(self):
        return self.tx_power_steps[self.curr_tx_pow]

    def get_tx_pow_step(self):
        return self.curr_tx_pow

    def set_tx_pow(self, tx_power_step):
        self.curr_tx_pow = tx_power_step

    def get_RSSI_to_dst(self):
        return self.RSSI_to_dst
    
    def set_RSSI_to_dst(self, rssi):
        self.RSSI_to_dst = rssi
        self.set_fade_margin_to_dst(rssi - self.get_rx_sensitivity())
    
    def get_fade_margin_to_dst(self):
        return self.fade_margin_to_dst

    def set_fade_margin_to_dst(self, margin):
        self.fade_margin_to_dst = margin
        '''
        if (self.fade_margin_to_dst > 0) and (self.get_snr() > 0):
            self.link_status = 1
        else:
            self.link_status = 0
        '''

    def set_link_status(self, link_status):
        self.link_status = link_status

    def get_link_status(self):
        return self.link_status

    def get_bit_rate(self):
        return self.bit_rate
    
    def get_snr(self):
        return self.snr

    def set_norm_bit_rate(self, bit_rate):
        self.norm_bit_rate = bit_rate

    def get_norm_bit_rate(self):
        return self.norm_bit_rate
    
    def get_radio_str(self):
        return self.radio_str

    def set_tx_pow_hw(self, tx_pow):
        self.curr_tx_pow = tx_pow
        self.tx_pow_set_cb(self.tx_pow_map[self.curr_tx_pow])

    def set_norm_pwr_steps(self, norm_pwr_steps):
        self.norm_pwr_steps = norm_pwr_steps
    
    def get_norm_pwr_steps(self):
        return self.norm_pwr_steps