

from enum import IntEnum

class PKT_TYPE(IntEnum):
    PKT_EMPTY       = 0
    PKT_STATUS      = 1
    PKT_SWITCH      = 2
    PKT_DATA        = 3



class MultiRadioPkt():
    PKT_TYPE        = 0
    RADIO_ID        = 1
    LEN             = 2
    DATA            = 3
    FOOTER          = DATA+LEN



class MultiRadioControlPkt():

    def __init__(self):
        self.pkt = bytearray(5)


    def decode_packet(self, pkt_bytes):
        pass

    def build_pkt_status(self):
        pass

    def build_pkt_switch(self):
        pass


class MultiRadioDataPkt():

    def __init__(self):
        pass

    def decode_packet(self, pkt_bytes):
        pass

    def build_pkt(self):
        pass
