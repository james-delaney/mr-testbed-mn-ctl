from spaces import RLActionSpace_Radio, RLStateSpace_Radio, RLActionSpace_RadioPwr, RLStateSpace_RadioPwr

class RLRadioCaps():

    def __init__(self, bitrate, power_levels):
        self.bitrate = bitrate
        self.power_levels = power_levels
    
    def get_bitrate(self):
        return self.bitrate

    def get_power_levels(self):
        return self.power_levels


class RLRadioEnvironment():

    def __init__(self, action_space):
        self.radios = []
        self.num_radios = 0

        self.curr_radio_id = 0

        self.action_space = action_space

    # returns the current observations/state of the environment
    def env_step(self, action):
        if isinstance(self.action_space , RLActionSpace_Radio):
            if action == RLActionSpace_Radio.ACTION_RADIO_0:
                #print('env step r0')
                self.curr_radio_id = 0
                self.curr_radio = self.radios[0]
                self.alt_radio = self.radios[1]
            elif action == RLActionSpace_Radio.ACTION_RADIO_1:
                #sprint('env step r1')
                self.curr_radio_id = 1
                self.curr_radio = self.radios[1]
                self.alt_radio = self.radios[0]
            elif action == RLActionSpace_Radio.NO_TX:
                self.curr_radio_id = -1
        elif isinstance(self.action_space, RLActionSpace_RadioPwr):
            if action >= RLActionSpace_RadioPwr.RADIO_0_P_0 and action <= RLActionSpace_RadioPwr.RADIO_0_P_4:
                self.curr_radio_id = 0
                self.curr_radio = self.radios[0]
                self.alt_radio = self.radios[1]
                self.curr_radio.set_tx_pow(action)
            elif action >= RLActionSpace_RadioPwr.RADIO_1_P_0 and action <= RLActionSpace_RadioPwr.RADIO_1_P_4:
                self.curr_radio_id = 1
                self.curr_radio = self.radios[1]
                self.alt_radio = self.radios[0]
                self.curr_radio.set_tx_pow(action-5)


        #alt_radio_id = self.curr_radio_id-1

        #curr_radio = self.radios[self.curr_radio_id]
        #alt_radio = self.radios[alt_radio_id]

        reward = self.calc_reward(self.curr_radio, self.alt_radio, action)
        
        return self.curr_radio, reward, False, None


    def calc_reward(self, curr_radio, alt_radio, action):
        r = 0

        # check for the case where we should not be transmitting
        if action == self.action_space.NO_TX:
            if (not curr_radio.get_link_status()) and  (not alt_radio.get_link_status()):
                r = 1
                #return r
            elif (curr_radio.get_link_status() or alt_radio.get_link_status()):
                r = -1
        # check if the other radio has a higher bitrate
        elif (alt_radio.get_bit_rate() > curr_radio.get_bit_rate()): # 17/09/2020
            if alt_radio.get_link_status():
                # if so, and the other radio has a link available, give a penalty
                r = -1
            else:
                # otherwise, check that status of the current radio and give a proportionately higher
                #   reward if the link is up
                if (curr_radio.get_link_status()):
                    r = (1-(curr_radio.get_norm_bit_rate()))
                else:
                    r = -1       
        else:
            # catch the final case where the current radio is the best available (highest bitrate
            #   and link is up)
            if (curr_radio.get_link_status()):
                    r = (curr_radio.get_norm_bit_rate())
            else:
                r = -1

        #print('reward: ', r, curr_radio.get_radio_id(), curr_radio.get_link_status(), alt_radio.get_link_status(), action)
        return r

    # register a new radio into the RL environment
    # returns the ID of the new radio
    def reg_radio(self, radio):
        self.radios.append(radio)
        self.num_radios += 1
        return self.num_radios
        

    # update the radio state parameters for the specified radio_id
    def update_radio_obs(self, radio_id, rssi):
        pass

    def set_reward_func(self):
        pass

class MO_RLRadioEnvironment():

    def __init__(self, action_space):
        self.radios = []
        self.num_radios = 0

        self.curr_radio_id = 0

        self.num_objs = 2

        self.action_space = action_space

    # returns the current observations/state of the environment
    def env_step(self, action):
        if isinstance(self.action_space , RLActionSpace_Radio):
            if action == RLActionSpace_Radio.ACTION_RADIO_0:
                #print('env step r0')
                self.curr_radio_id = 0
                self.curr_radio = self.radios[0]
                self.alt_radio = self.radios[1]
            elif action == RLActionSpace_Radio.ACTION_RADIO_1:
                #sprint('env step r1')
                self.curr_radio_id = 1
                self.curr_radio = self.radios[1]
                self.alt_radio = self.radios[0]
            elif action == RLActionSpace_Radio.NO_TX:
                self.curr_radio_id = -1
        elif isinstance(self.action_space, RLActionSpace_RadioPwr):
            if action >= RLActionSpace_RadioPwr.RADIO_0_P_0 and action <= RLActionSpace_RadioPwr.RADIO_0_P_4:
                self.curr_radio_id = 0
                self.curr_radio = self.radios[0]
                self.alt_radio = self.radios[1]
                self.curr_radio.set_tx_pow(action)
            elif action >= RLActionSpace_RadioPwr.RADIO_1_P_0 and action <= RLActionSpace_RadioPwr.RADIO_1_P_4:
                self.curr_radio_id = 0
                self.curr_radio = self.radios[1]
                self.alt_radio = self.radios[0]
                self.curr_radio.set_tx_pow(action-5)


        #alt_radio_id = self.curr_radio_id-1

        #curr_radio = self.radios[self.curr_radio_id]
        #alt_radio = self.radios[alt_radio_id]

        reward = self.calc_reward(self.curr_radio, self.alt_radio, action)
        
        return self.curr_radio, reward, False, None
    
    def calc_reward(self, curr_radio, alt_radio, action):
         reward = [self.rw_fn_bitrate(curr_radio, alt_radio, action), self.rw_fn_pwr_cons(curr_radio, alt_radio, action)]

         return reward

    def rw_fn_bitrate(self, curr_radio, alt_radio, action):
        r = 0

        # check for the case where we should not be transmitting
        if action == RLActionSpace_RadioPwr.NO_TX:
            if (not curr_radio.get_link_status()) and  (not alt_radio.get_link_status()):
                r = 1
                return r
            elif (curr_radio.get_link_status() or alt_radio.get_link_status()):
                r = -1
        # check if the other radio has a higher bitrate
        elif (alt_radio.get_bit_rate() > curr_radio.get_bit_rate()): # 17/09/2020
            if alt_radio.get_link_status():
                # if so, and the other radio has a link available, give a penalty
                r = -1
            else:
                # otherwise, check that status of the current radio and give a proportionately higher
                #   reward if the link is up
                if (curr_radio.get_link_status()):
                    r = (1-(curr_radio.get_norm_bit_rate()))
                    #r = curr_radio.get_norm_bit_rate()
                else:
                    r = -1       
        else:
            # catch the final case where the current radio is the best available (highest bitrate
            #   and link is up)
            if (curr_radio.get_link_status()):
                r = (curr_radio.get_norm_bit_rate())
            else:
                r = -1
        return r

    def rw_fn_pwr_cons(self, curr_radio, alt_radio, action):
        r = 0

        curr_pow_step = curr_radio.get_tx_pow_step()
        #translate current power step into range of all power levels
        actual_pow_step = curr_pow_step
        if (curr_radio.get_radio_id() > 0):
            curr_pow_step += (5 * curr_radio.get_radio_id())
        if curr_pow_step > 0:
            if (curr_radio.get_link_status()):
                # check if we are on the lowest power level of the higher power radio - if so, est_fm is the fade margin of the alt_radio at its highest power level
                if (curr_radio.get_radio_id() > 0) and (actual_pow_step == 0):
                    est_fm = alt_radio.get_fade_margin_to_dst()
                else:
                    est_fm = curr_radio.get_fade_margin_to_dst() - (curr_radio.tx_power_steps[actual_pow_step] - curr_radio.tx_power_steps[actual_pow_step-1])   # estimated fade margin if we use a lower pwr lvl
                if (est_fm > 3):
                    # proportionately negative reward if there is an available lower power level
                    r = -1 * curr_radio.get_norm_pwr_steps()[curr_pow_step]
                else:
                    r = (1 - curr_radio.get_norm_pwr_steps()[curr_pow_step])
                    #r = -1 * curr_radio.get_norm_pwr_steps()[curr_pow_step]  # added 16/11/2020
            # check to handle to case where we are out of range and should not be transmitting
            elif (not curr_radio.get_link_status()) and (curr_radio.get_tx_pow_step() == 4) and (curr_radio.get_radio_id() == 0) and (action == RLActionSpace_RadioPwr.NO_TX):
                r = 1
            else:
                r = -1
        else:
            if (curr_radio.get_link_status()):
                r = (1 - curr_radio.get_norm_pwr_steps()[curr_pow_step])
                #r = -1 * curr_radio.get_norm_pwr_steps()[curr_pow_step] # makes behaviour from w1=0.25 or more work
            # check to handle to case where we are out of range and should not be transmitting
            elif (not curr_radio.get_link_status()) and (curr_radio.get_tx_pow_step() == 4) and (curr_radio.get_radio_id() == 0) and (action == RLActionSpace_RadioPwr.NO_TX):
                r = 1
            else:
                r = -1
    
        return r

    # register a new radio into the RL environment
    # returns the ID of the new radio
    def reg_radio(self, radio):
        self.radios.append(radio)
        self.num_radios += 1
        return self.num_radios

    