
import _thread
from rl_radio import RLRadio
from wifi_link_ctl import WiFiLinkCtl, WiFiEvt
from lora_link_ctl import LoRaLinkCtl, LoRaEvt
from rl_env import RLRadioEnvironment, MO_RLRadioEnvironment
from rl_agent import RLAgent, RLAdaptiveExpAgent, MO_RLAgent
import rl_agent
import time
import utime
import io
from machine import Timer
from uart_interface import UARTMsg, UARTInterface
import uart_interface
from spaces import *

RADIO_ID_WIFI               = 1
RADIO_ID_LORA               = 0
RADIO_ID_OFF                = 2

class CommsCtlPkt():
    PKT_ID                  = 0
    DATA                    = 1



PKT_ID_STATUS               = 0
PKT_ID_TRACE                = 1
PKT_ID_DATA                 = 2


class CommsCtl():

    def sample_t(self):
        print('new thread')


    def __init__(self):
        self.wifi_link_layer = WiFiLinkCtl(False, [0x00, 0x01], 8, 'abcd1234', 100)
        self.wifi_link_layer.reg_upper_cb(self._handle_wifi_pkt)
        self.wifi_link_layer.reg_evt_cb(self._handle_wifi_evt)

        self.lora_link_layer = LoRaLinkCtl(False, [0x00, 0x01], 916800000, 9, 100)
        self.lora_link_layer.reg_upper_cb(self._handle_lora_pkt)
        self.lora_link_layer.reg_evt_cb(self._handle_lora_evt)

        self.action_space = RLActionSpace_RadioPwr()
        self.state_space = RLStateSpace_RadioPwr()

        self.rl_env = MO_RLRadioEnvironment(self.action_space)
        #self.wifi_radio = RLRadio(RADIO_ID_WIFI, 'wifi', 2400, 20000, -90, 1000, [0, 2, 6, 11, 14], [], self.wifi_link_layer.set_tx_pow_lvl, [[78,127], [44,51], [28,33], [8,19], [-128,7 ]])
        #self.wifi_radio = RLRadio(RADIO_ID_WIFI, 'wifi', 2400, 20000, -90, 1000, [0, 2, 6, 11, 14], [], self.wifi_link_layer.set_tx_pow_lvl, [48, 65, 70, 76, 100])
        self.wifi_radio = RLRadio(RADIO_ID_WIFI, 'wifi', 2400, 20000, -90, 1000, [2, 7, 11, 15, 20], [], self.wifi_link_layer.set_tx_pow_lvl, [8, 28, 44, 60, 80])
        self.wifi_radio.set_norm_bit_rate(1.0)
        self.lora_radio = RLRadio(RADIO_ID_LORA, 'lora', 915, 125, -120, 1.7, [5,8,10,12,14], [10.66,11.75,13.93,20.55,37.16], self.lora_link_layer.set_tx_pow_lvl, [5,8,10,12,14])
        self.lora_radio.set_norm_bit_rate(0.125)

        self.rl_env.reg_radio(self.lora_radio)
        self.rl_env.reg_radio(self.wifi_radio)        

        self.radios = [self.lora_radio, self.wifi_radio]
        self.active_radio = self.wifi_radio
        self.active_radio_id = self.wifi_radio.get_radio_id()

        #self.rl_agent = RLAgent(self.state_space, self.action_space, self.rl_env.env_step, self.agent_step)
        self.rl_agent = MO_RLAgent(self.state_space, self.action_space, self.rl_env.env_step, self.agent_step, self.log_step, 2, [0.5,0.5])

        self.send_pkt_lock = _thread.allocate_lock()
        self.send_pkt_buf = []
        self.send_pkt_buf_size = 0

        self.recv_pkt_lock = _thread.allocate_lock()
        self.recv_pkt_buf = []
        self.recv_pkt_buf_size = 0

        self.uart_msg = UARTInterface(UARTMsg(uart_interface.ctl_msg), receiver=True, uart_iface=2, baudrate=115200, msg_rx_cb=self.handle_uart_ctl_msg)
        self.log_msg = UARTMsg(uart_interface.min_log_msg)

        print('starting comms_ctl thread')
        _thread.start_new_thread(self._comms_ctl_thread, ())
        print('started comms_ctl thread')

        # no display needed now on MN board
        #_thread.start_new_thread(self._disp_update_thread, ())

        self.set_report_stats_int()

    def _comms_ctl_thread(self):
        print('inside comms_ctl thread')
        while (not self.wifi_link_layer.get_init_status()) or (not self.lora_link_layer.get_init_status()):
            print('init wait loop')
        
        #print('starting rl agent')
        #self.rl_agent.set_agent_active()
        #print('rl agent started')
        while True:
            #print('comms ctl tick')
            
            wifi_link, wifi_rssi, wifi_snr = self.wifi_link_layer.get_link_stats()
            lora_link, lora_rssi, lora_snr = self.lora_link_layer.get_link_stats()

            self.radios[RADIO_ID_WIFI].set_link_status(wifi_link)
            self.radios[RADIO_ID_LORA].set_link_status(lora_link)

            '''
            print('RSSI - wifi: ' + str(wifi_rssi) + ' lora: ' + str(lora_rssi))
            if self.active_radio is not None:
                print('active radio: ' + str(self.active_radio.get_radio_str()))
            else:
                print('active radio: Radio off')
            print()
            '''
            self.radios[RADIO_ID_WIFI].set_RSSI_to_dst(wifi_rssi)
            self.radios[RADIO_ID_LORA].set_RSSI_to_dst(lora_rssi)
            
            #if not self.rl_agent.active:
            #    self.uart_msg.tx_uart_msg(self.log_msg.build_msg(timestamp=0, wifi_link=wifi_link, lora_link=lora_link, wifi_rssi=wifi_rssi, lora_rssi=lora_rssi, active_radio=2))
            
            time.sleep(0.25)

        return 0

    def handle_uart_ctl_msg(self, msg):
        cmd = int.from_bytes(msg['cmd'], 'big')
        data = int.from_bytes(msg['data'], 'big')

        if data == uart_interface.MSG_DATA_W1_000:
            self.rl_agent.set_w1(0)
        elif data == uart_interface.MSG_DATA_W1_025:
            self.rl_agent.set_w1(0.1)
        elif data == uart_interface.MSG_DATA_W1_050:
            self.rl_agent.set_w1(0.5)
        elif data == uart_interface.MSG_DATA_W1_075:
            self.rl_agent.set_w1(0.75)
        elif data == uart_interface.MSG_DATA_W1_100:
            self.rl_agent.set_w1(1)


        if cmd == uart_interface.MSG_START:
            print('msg start rx')
            self.rl_agent.set_agent_active(MO_RLAgent.EXPL_FIXED)
            self.stats_int.cancel()
        elif cmd == uart_interface.MSG_START_FIXED:
            print('msg start fixed expl rx')
            self.rl_agent.set_agent_active(MO_RLAgent.EXPL_FIXED)
            self.stats_int.cancel()
        elif cmd == uart_interface.MSG_START_DECAY:
            print('msg start decay expl rx')
            self.rl_agent.set_agent_active(MO_RLAgent.EXPL_DECAY)
            self.stats_int.cancel()
        elif cmd == uart_interface.MSG_START_ADAPTV:
            print('msg start adaptv expl rx')
            self.rl_agent.set_agent_active(MO_RLAgent.EXPL_ADAPTV)
            self.stats_int.cancel()
        elif cmd == uart_interface.MSG_STOP:
            print('msg stop rx')
            self.rl_agent.set_agent_inactive()
            self.set_report_stats_int()
        else:
            print('invalid ctl msg rx')

    def set_report_stats_int(self):
        self.stats_int = Timer.Alarm(self.report_stats_int, 1, periodic=True)

    def _disp_update_thread(self):
        while True:
            io.update_display(self.wifi_link_layer.get_link_stats(), self.lora_link_layer.get_link_stats(), self.active_radio_id)
            time.sleep(1.0)

    def _handle_wifi_pkt(self, pkt, pkt_len):
        pkt_id = pkt[0]

        if (pkt_id == PKT_ID_STATUS):
            self.handle_status_pkt(RADIO_ID_WIFI, pkt, pkt_len)
        elif (pkt_id == PKT_ID_TRACE):
            self.handle_trace_pkt(RADIO_ID_WIFI, pkt, pkt_len)
        elif (pkt_id == PKT_ID_DATA):
            self.handle_data_pkt(RADIO_ID_WIFI, pkt, pkt_len)

    def _handle_wifi_evt(self, evt):
        #print('evt val: ', evt, WiFiEvt.WIFI_EVT_LINK_DN, evt==WiFiEvt.WIFI_EVT_LINK_DN)
        if evt == WiFiEvt.WIFI_EVT_LINK_DN: 
            #print('evt val: ', evt, WiFiEvt.WIFI_EVT_LINK_DN, evt==WiFiEvt.WIFI_EVT_LINK_DN)
            self.radios[RADIO_ID_WIFI].set_link_status(0)
            ## this should trigger learning algorithm to step if this is the active radio
            #print('EVT: wifi down')
            self.radios[RADIO_ID_WIFI].set_tx_pow_hw(4)
        elif evt == WiFiEvt.WIFI_EVT_LINK_UP:
            self.radios[RADIO_ID_WIFI].set_link_status(1)
            #print('EVT: wifi up')


    def _handle_lora_pkt(self, pkt, pkt_len):
        pkt_id = pkt[0]

        if (pkt_id == PKT_ID_STATUS):
            self.handle_status_pkt(RADIO_ID_LORA, pkt, pkt_len)
        elif (pkt_id == PKT_ID_TRACE):
            self.handle_trace_pkt(RADIO_ID_LORA, pkt, pkt_len)
        elif (pkt_id == PKT_ID_DATA):
            self.handle_data_pkt(RADIO_ID_LORA, pkt, pkt_len)

    def _handle_lora_evt(self, evt):
        if (evt == LoRaEvt.LORA_EVT_LINK_DN):
            self.radios[RADIO_ID_LORA].set_link_status(0)
            self.radios[RADIO_ID_LORA].set_tx_pow_hw(4)
        elif (evt == LoRaEvt.LORA_EVT_LINK_UP):
            self.radios[RADIO_ID_LORA].set_link_status(1)

    def get_active_radio(self):
        return self.active_radio

    def agent_step(self, active_radio_id, radio_off, pwr_lvl=None):
        #print('setting active radio to ', active_radio_id)
        if not radio_off:
            self.active_radio_id  = active_radio_id
            self.active_radio = self.radios[active_radio_id]
            if pwr_lvl is not None:
                #print('setting pwr lvl to ', pwr_lvl)
                self.active_radio.set_tx_pow_hw(pwr_lvl)
        else:
            self.active_radio_id = -1
            self.active_radio = None

        '''
        wifi_link, wifi_rssi, wifi_snr = self.wifi_link_layer.get_link_stats()
        lora_link, lora_rssi, lora_snr = self.lora_link_layer.get_link_stats()

        wifi_pwr = self.radios[1].get_tx_pow_step()
        lora_pwr = self.radios[0].get_tx_pow_step()

        if self.rl_agent.active:
            #print('step log ', self.rl_agent.n_steps)
            self.uart_msg.tx_uart_msg(self.log_msg.build_msg(timestamp=self.rl_agent.n_steps, wifi_link=wifi_link, lora_link=lora_link, wifi_rssi=wifi_rssi, lora_rssi=lora_rssi, wifi_pwr=wifi_pwr, lora_pwr=lora_pwr,active_radio=self.active_radio_id))
        '''
    
    def log_step(self):
        wifi_link, wifi_rssi, wifi_snr = self.wifi_link_layer.get_link_stats()
        lora_link, lora_rssi, lora_snr = self.lora_link_layer.get_link_stats()

        wifi_pwr = self.radios[1].get_tx_pow_step()
        lora_pwr = self.radios[0].get_tx_pow_step()

        if self.rl_agent.active:
            #print('step log ', self.rl_agent.n_steps)
            self.uart_msg.tx_uart_msg(self.log_msg.build_msg(timestamp=self.rl_agent.n_steps, wifi_link=wifi_link, lora_link=lora_link, wifi_rssi=wifi_rssi, lora_rssi=lora_rssi, wifi_pwr=wifi_pwr, lora_pwr=lora_pwr,active_radio=self.active_radio_id))
            #print(self.rl_agent.epsilon[self.rl_agent.s_prime])

    def report_stats_int(self, arg):
        #print('report stats int')
        wifi_link, wifi_rssi, wifi_snr = self.wifi_link_layer.get_link_stats()
        lora_link, lora_rssi, lora_snr = self.lora_link_layer.get_link_stats()
        self.uart_msg.tx_uart_msg(self.log_msg.build_msg(timestamp=0, wifi_link=wifi_link, lora_link=lora_link, wifi_rssi=wifi_rssi, lora_rssi=lora_rssi, wifi_pwr=-1, lora_pwr=-1, active_radio=2))


        

    def _queue_send_data(self, data, dst):
        if (self.active_radio.get_radio_id() == RADIO_ID_WIFI):
            self.wifi_link_layer.send_data(data, len(data), dst)
        elif (self.active_radio.get_radio_id() == RADIO_ID_LORA):
            self.lora_link_layer.send_data(data, len(data), dst)
        elif (self.active_radio == None):
            # raise radio off evt - queue pkt to be sent
            return


    def handle_status_pkt(self, radio_id, data, pkt_len):
        pass

    def handle_trace_pkt(self, radio_id, data, pkt_len):
        pass

    def handle_data_pkt(self, radio_id, data, pkt_len):
        pass

