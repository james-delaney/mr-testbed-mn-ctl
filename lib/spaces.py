from util import randrange

class RLStateSpace():

    def __init__(self):
        self.num_states = 0

    def state_space_size(self):
        return self.num_states

    def get_curr_state(self, action_space, curr_radio, action):
        pass


class RLActionSpace():

    def __init__(self):
        self.num_actions = 0

    def action_space_size(self):
        return self.num_actions

    def sample(self):
        return randrange(0, self.action_space_size()-1)


class RLActionSpace_Radio(RLActionSpace):

    ACTION_RADIO_0                  = 0
    ACTION_RADIO_1                  = 1
    NO_TX                           = 2
    SIZE                            = 3

    def __init__(self):
        self.num_actions = int(RLActionSpace_Radio.SIZE)

    def action_space_size(self):
        return self.num_actions

class RLActionSpace_RadioPwr(RLActionSpace):

    RADIO_0_P_0              = 0
    RADIO_0_P_1              = 1
    RADIO_0_P_2              = 2
    RADIO_0_P_3              = 3
    RADIO_0_P_4              = 4
    RADIO_1_P_0              = 5
    RADIO_1_P_1              = 6
    RADIO_1_P_2              = 7
    RADIO_1_P_3              = 8
    RADIO_1_P_4              = 9
    NO_TX                    = 10
    SIZE                     = 11

    def __init__(self):
        self.num_actions = int(RLActionSpace_RadioPwr.SIZE)

    def action_space_size(self):
        return self.num_actions

class RLStateSpace_Radio(RLStateSpace):

    STATE_RADIO_0                   = 0
    STATE_RADIO_1                   = 1
    STATE_RADIO_OFF                 = 2
    SIZE                            = 3

    def __init__(self):
        self.num_states = int(RLStateSpace_Radio.SIZE)

    def get_curr_state(self, action_space, curr_radio, action):
        curr_radio_id = curr_radio.get_radio_id()

        if action != action_space.NO_TX:
            if curr_radio_id == 0:
                return 0
            elif curr_radio_id == 1:
                return 1
        else:
            #print('gcs: action: ', action)
            return 2

class RLStateSpace_RadioPwr(RLStateSpace):

    RADIO_0_TX_POW_0        = 0
    RADIO_0_TX_POW_1        = 1
    RADIO_0_TX_POW_2        = 2
    RADIO_0_TX_POW_3        = 3
    RADIO_0_TX_POW_4        = 4
    RADIO_1_TX_POW_0        = 5
    RADIO_1_TX_POW_1        = 6
    RADIO_1_TX_POW_2        = 7
    RADIO_1_TX_POW_3        = 8
    RADIO_1_TX_POW_4        = 9
    RADIO_OFF               = 10
    RADIO_0_LINK_DN         = 11
    RADIO_1_LINK_DN         = 12
    SIZE                    = 13

    def __init__(self):
        self.num_states = RLStateSpace_RadioPwr.SIZE

    def get_curr_state(self, action_space, curr_radio, action):
        state = 0

        curr_radio_id = curr_radio.get_radio_id()
        curr_tx_pow = curr_radio.get_tx_pow_step()

        if action == action_space.NO_TX:
            state = RLStateSpace_RadioPwr.RADIO_OFF
        elif curr_radio_id == 0:
            if curr_tx_pow == 0:
                state = RLStateSpace_RadioPwr.RADIO_0_TX_POW_0
            elif curr_tx_pow == 1:
                state = RLStateSpace_RadioPwr.RADIO_0_TX_POW_1
            elif curr_tx_pow == 2:
                state = RLStateSpace_RadioPwr.RADIO_0_TX_POW_2
            elif curr_tx_pow == 3:
                state = RLStateSpace_RadioPwr.RADIO_0_TX_POW_3
            elif curr_tx_pow == 4:
                state = RLStateSpace_RadioPwr.RADIO_0_TX_POW_4
        elif curr_radio_id == 1:
            if curr_tx_pow == 0:
                state = RLStateSpace_RadioPwr.RADIO_1_TX_POW_0
            elif curr_tx_pow == 1:
                state = RLStateSpace_RadioPwr.RADIO_1_TX_POW_1
            elif curr_tx_pow == 2:
                state = RLStateSpace_RadioPwr.RADIO_1_TX_POW_2
            elif curr_tx_pow == 3:
                state = RLStateSpace_RadioPwr.RADIO_1_TX_POW_3
            elif curr_tx_pow == 4:
                state = RLStateSpace_RadioPwr.RADIO_1_TX_POW_4
        else:
            # should never get here
            return -1
        
        return state


#class RLStateSpace_FadeMargin(RLStateSpace):