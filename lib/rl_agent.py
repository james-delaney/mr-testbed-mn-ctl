
import time
from machine import Timer
import machine

from spaces import *
import ulab as np
from ulab import vector
from ulab import numerical, linalg
import os
import _thread
import math

print('you are running ulab version', np.__version__)

ACTION_SPACE_ID_RADIO               = 0
ACTION_SPACE_ID_RADIO_PWR           = 1

RL_ALG_STEP_PERIOD = 0.1
RL_ALG_ACTION_PERIOD = 0.1
RL_ALG_ACTION_STEP_INT = RL_ALG_ACTION_PERIOD/RL_ALG_STEP_PERIOD

### RL Algorithm parameters:
ALPHA = 0.7
GAMMA = 0.1
EPSILON = 0.01

EXPL_SIGMA = 0.5
EXPL_DLT = 0.09090909
#SIGMA = 0.01


def exp_td_err(lr, td_err, sigma):
    num  = -1 * abs(lr * td_err)
    #print(td_err)
    return vector.exp(num/sigma)

def func_eps(lr, td_err, sigma):
    e = exp_td_err(lr, td_err, sigma)

    return ((1 - e)/(1 + e))


class RLAgent():

    def __init__(self, state_space, action_space, env_step_cb, action_select_cb):
        self.state_space = state_space
        self.action_space = action_space


        self.q_table = np.zeros((self.state_space.state_space_size(), self.action_space.action_space_size()))
        #print(self.q_table.shape())
        #print(self.q_table[2,2])
        self.env_step_cb = env_step_cb
        self.action_select_cb = action_select_cb
        self.active = False
        self.n_steps = 0

        self.s = 0
        self.a = 0
        self.s_prime = 0
        self.a_prime = 0
        self.last_action = 0
        self.curr_radio = None
        self.last_action_lock = _thread.allocate_lock()

        print('rl agent init')

    def set_agent_active(self):
        self.active = True
        self.set_step_interrupt(RL_ALG_STEP_PERIOD, RL_ALG_ACTION_PERIOD)
        self.reset_mem()

    def set_agent_inactive(self):
        if self.active:
            self.active = False
            self.step_int.cancel()
    
    # trigger a step in the environment and take a learning step
    def _agent_ctl_step(self, alarm):
        #print('agent step', self.s, self.a, self.s_prime, self.a_prime, machine.rng())
        #print('start_step')

        self.last_action_lock.acquire()
        self.curr_radio, r, done, info = self.env_step_cb(self.a)
        #self._agent_action(curr_radio.get_radio_id(), self.a)

        self.s_prime = self.state_space.get_curr_state(self.action_space, self.curr_radio, self.a)
        self.last_action_lock.release()

        #print(machine.rng()/100000000)
        if ((machine.rng()/100000000) < EPSILON):
            self.a_prime = self.action_space.sample()
            #print(self.a_prime)
        else:
            self.a_prime = numerical.argmax(self.q_table[self.s_prime])
            #print('r '+str(self.a_prime))
        #print(self.q_table)
        #print('SARSA: ', self.s, self.a, r, self.s_prime, self.a_prime)
        td_err = (r + GAMMA*self.q_table[self.s_prime, self.a_prime] - self.q_table[self.s, self.a])
        new_q = self.q_table[self.s, self.a] + (ALPHA * td_err)

        self.q_table[self.s, self.a] = new_q

        self.s = self.s_prime 
        self.a = self.a_prime
        self.last_action_lock.acquire()
        self.last_action = self.a
        self.last_action_lock.release()

        self.n_steps += 1


    def _agent_ctl_action(self):
        #print('action sel step')
        self.last_action_lock.acquire()
        self._agent_action(self.curr_radio.get_radio_id(), self.last_action)
        self.last_action_lock.release()

    # set alarm for periodic step interrupt
    def set_step_interrupt(self, step_time, action_time):
        self.step_int = Timer.Alarm(self._agent_ctl_step, step_time, periodic=True)
        #self.action_int = Timer.Alarm(self._agent_ctl_action, action_time, periodic=True)

    # interrupt the waiting time step and force agent to step the environmnet
    def agent_interrupt_step(self):
        pass

    def _agent_action(self, curr_radio_id, a):
        if isinstance(self.action_space, RLActionSpace_Radio):
            if a == self.action_space.ACTION_RADIO_0:
                self.action_select_cb(curr_radio_id, False)
            elif a == self.action_space.ACTION_RADIO_1:
                self.action_select_cb(curr_radio_id, False)
            elif a == self.action_space.ACTION_RADIO_OFF:
                self.action_select_cb(curr_radio_id, True)
        elif isinstance(self.action_space, RLActionSpace_RadioPwr):
            if curr_radio_id == 0:
                if a == self.action_space.RADIO_0_P_0:
                    self.action_select_cb(curr_radio_id, False, 0)
                elif a == self.action_space.RADIO_0_P_1:
                    self.action_select_cb(curr_radio_id, False, 1)
                elif a == self.action_space.RADIO_0_P_2:
                    self.action_select_cb(curr_radio_id, False, 2)
                elif a == self.action_space.RADIO_0_P_3:
                    self.action_select_cb(curr_radio_id, False, 3)
                elif a == self.action_space.RADIO_0_P_4:
                    self.action_select_cb(curr_radio_id, False, 4)
            elif curr_radio_id == 1:
                if a == self.action_space.RADIO_1_P_0:
                    self.action_select_cb(curr_radio_id, False, 0)
                elif a == self.action_space.RADIO_1_P_1:
                    self.action_select_cb(curr_radio_id, False, 1)
                elif a == self.action_space.RADIO_1_P_2:
                    self.action_select_cb(curr_radio_id, False, 2)
                elif a == self.action_space.RADIO_1_P_3:
                    self.action_select_cb(curr_radio_id, False, 3)
                elif a == self.action_space.RADIO_1_P_4:
                    self.action_select_cb(curr_radio_id, False, 4)


    def reset_mem(self):
        self.q_table = np.zeros((self.state_space.state_space_size(), self.action_space.action_space_size()))
        self.n_steps = 0

class RLAdaptiveExpAgent():
    
    def __init__(self, state_space, action_space, env_step_cb, action_select_cb):
        self.active = False
        self.state_space = state_space
        self.action_space = action_space
        
        self.q_table = np.zeros((self.state_space.state_space_size(), self.action_space.action_space_size()))
        print(self.q_table.shape())
        print(self.q_table[2,2])
        self.env_step_cb = env_step_cb
        self.action_select_cb = action_select_cb
        self.active = False
        self.n_steps = 0

        self.epsilon = np.ones(self.state_space.state_space_size())
        self.dlt = 1/self.action_space.action_space_size()

        self.s = 0
        self.a = 0
        self.s_prime = 0
        self.a_prime = 0

        print('rl agent init')

    def set_agent_active(self):
        self.active = True
        self.set_step_interrupt(RL_ALG_STEP_PERIOD)

    def set_agent_inactive(self):
        if self.active:
            self.active = False
            self.step_int.cancel()
    
    # trigger a step in the environment and take a learning step
    def _agent_ctl_step(self, alarm):
        #print('agent step', self.s, self.a, self.s_prime, self.a_prime, machine.rng())
        #print('start_step')
        curr_radio, r, done, info = self.env_step_cb(self.a)
        self._agent_action(curr_radio.get_radio_id(), self.a)

        self.s_prime = self.state_space.get_curr_state(self.action_space, curr_radio, self.a)

        #print('eps val ', self.epsilon[self.s])
        if ((machine.rng()/100000000) < self.epsilon[self.s]):
            self.a_prime = self.action_space.sample()
            #print('rand action', self.a_prime)
        else:
            self.a_prime = numerical.argmax(self.q_table[self.s_prime])
            #print('r '+str(self.a_prime))
        #print(self.q_table)
        #print('SARSA: ', self.s, self.a, r, self.s_prime, self.a_prime, curr_radio.get_radio_id())
        td_err = (r + GAMMA*self.q_table[self.s_prime, self.a_prime] - self.q_table[self.s, self.a])
        new_q = self.q_table[self.s, self.a] + (ALPHA * td_err)

        self.q_table[self.s, self.a] = new_q

        self.epsilon[self.s] = self.dlt * func_eps(ALPHA, td_err, EXPL_SIGMA) + (1 - self.dlt) * self.epsilon[self.s]

        self.s = self.s_prime 
        self.a = self.a_prime

        self.n_steps += 1

    # set alarm for periodic step interrupt
    def set_step_interrupt(self, step_time):
        self.step_int = Timer.Alarm(self._agent_ctl_step, step_time, periodic=True)

    # interrupt the waiting time step and force agent to step the environmnet
    def agent_interrupt_step(self):
        pass

    def _agent_action(self, curr_radio_id, a):
        if isinstance(self.action_space, RLActionSpace_Radio):
            if a == self.action_space.ACTION_RADIO_0:
                self.action_select_cb(curr_radio_id, False)
                #print('sel r0')
            elif a == self.action_space.ACTION_RADIO_1:
                self.action_select_cb(curr_radio_id, False)
                #print('sel r1')
            elif a == self.action_space.ACTION_RADIO_OFF:
                self.action_select_cb(curr_radio_id, True)

    def reset_mem(self):
        self.q_table = np.zeros((self.state_space.state_space_size(), self.action_space.action_space_size()))
        self.n_steps = 0

class MO_RLAgent():

    EXPL_FIXED = 0
    EXPL_DECAY = 1
    EXPL_ADAPTV = 2

    def __init__(self, state_space, action_space, env_step_cb, action_select_cb, log_cb, env_objs, obj_weights):
        self.state_space = state_space
        self.action_space = action_space
        self.n_components = int(env_objs)
        self.obj_weights = np.array(obj_weights)
        self.expl_mode = 0
        self.epsilon = 0

        
        #print(self.n_components, self.state_space.state_space_size(), self.action_space.action_space_size())
        self.q_table = [np.zeros((self.state_space.state_space_size(), self.action_space.action_space_size()), dtype=np.float) for i in range(self.n_components)]
        #print(self.q_table.shape())
        #print(self.q_table[2,2])
        self.env_step_cb = env_step_cb
        self.action_select_cb = action_select_cb
        self.log_cb = log_cb
        self.active = False
        self.n_steps = 0

        self.s = 0
        self.a = 0
        self.s_prime = 0
        self.a_prime = 0
        self.last_action = 0
        self.curr_radio = None
        self.last_action_lock = _thread.allocate_lock()

        self.td_err = np.zeros(self.n_components, dtype=np.float)
        self.gm_q = np.zeros(self.action_space.action_space_size(), dtype=np.float)

        print('rl agent init')

    def set_agent_active(self, expl_mode):
        self.active = True
        self.setup_expl_mode(expl_mode)
        self.set_step_interrupt(RL_ALG_STEP_PERIOD, RL_ALG_ACTION_PERIOD)
        self.reset_mem()

    def set_agent_inactive(self):
        if self.active:
            self.active = False
            self.step_int.cancel()
    
    def setup_expl_mode(self, expl_mode):
        print('expl mode ', expl_mode)
        self.expl_mode = expl_mode
        if self.expl_mode == self.EXPL_FIXED:
            self.epsilon = EPSILON
        elif self.expl_mode == self.EXPL_DECAY:
            self.epsilon = 1
        elif self.expl_mode == self.EXPL_ADAPTV:
            self.epsilon = np.ones(self.state_space.state_space_size(), dtype=np.float)

    # trigger a step in the environment and take a learning step
    def _agent_ctl_step(self, alarm):
        #print('agent step', self.s, self.a, self.s_prime, self.a_prime, machine.rng())
        #print('start_step')

        #self.last_action_lock.acquire()
        self.curr_radio, r, done, info = self.env_step_cb(self.a)
        #self._agent_action(self.curr_radio.get_radio_id(), self.a)

        self.s_prime = self.state_space.get_curr_state(self.action_space, self.curr_radio, self.a)
        #self.last_action_lock.release()
        #print(self.s_prime, r)

        w_q = [w * self.q_table[idx][:, :] for idx, w in enumerate(self.obj_weights)]
        gm_q = np.zeros(self.action_space.action_space_size(), dtype=np.float)
        for i in range(self.n_components):
            for a in range(self.action_space.action_space_size()):
                gm_q[a] += w_q[i][self.s_prime, a][0]
            #gm_q = numerical.add(gm_q, w_q[i][self.s_prime, :])
            #gm_q += np.array([w_q[i][self.s_prime, a] for a in range(self.action_space.action_space_size())])
            #gm_q += np.array([w_q[i][self.s_prime, a][0] for a in range(self.action_space.action_space_size())])
            
        #print(np.array([np.array([1,1,1]), np.array([2,2,2])]))
        #gm_q = numerical.sum(np.array(gm_q))
        
        #gm_q = np.array([[(w_q[i][self.s_prime, a]) for a in range(self.action_space.action_space_size())] for i in range(self.n_components)])
        #print(self.td_err, r)
        wtde = linalg.dot(self.obj_weights, self.td_err)
        #print(wtde)
        self.a_prime = self.get_action(gm_q, self.n_steps, self.s_prime, wtde)
        #print(machine.rng()/100000000)
        
            #print('r '+str(self.a_prime))
        #print(self.q_table)
        #print('SARSA: ', self.s, self.a, r, self.s_prime, self.a_prime)
        for i in range(self.n_components):
            self.td_err[i] = (r[i] + GAMMA*self.q_table[i][self.s_prime, self.a_prime] - self.q_table[i][self.s, self.a])
            #print(r[i], GAMMA*self.q_table[i][self.s_prime, self.a_prime], self.q_table[i][self.s, self.a])
            new_q = self.q_table[i][self.s, self.a] + (ALPHA * self.td_err[i])

            self.q_table[i][self.s, self.a] = new_q

        self.s = self.s_prime 
        self.a = self.a_prime
        #self.last_action_lock.acquire()
        self.last_action = self.a
        #self.last_action_lock.release()

        if self.n_steps%RL_ALG_ACTION_STEP_INT == 0:
            self._agent_ctl_action()
        #print(self.n_steps)
        
        self.n_steps += 1


    def eps_greedy_action(self, gm_q, s, epsilon):
        #print(gm_q, s, epsilon)
        #if ((machine.rng()/100000000) < epsilon):
        if (os.urandom(1)[0]/255 < epsilon):
            #print('rand')
            return self.action_space.sample()
            #print(self.a_prime)
        else:
            #print('exploit')
            return numerical.argmax(gm_q)

    def get_action(self, gm_q, n_steps, s, wtde):
        if self.expl_mode == self.EXPL_FIXED:
            return self.eps_greedy_action(gm_q, s, self.epsilon)
        elif self.expl_mode == self.EXPL_DECAY:
            a = self.eps_greedy_action(gm_q, s, self.epsilon)
            if self.epsilon > 0.05:
                self.epsilon = math.pow(0.5, n_steps)
            else:
                self.epsilon = 0.05
            return a
        elif self.expl_mode == self.EXPL_ADAPTV:
            a = self.eps_greedy_action(gm_q, s, self.epsilon[s])
            self.epsilon[s] = EXPL_DLT * func_eps(ALPHA, wtde, EXPL_SIGMA) + (1 - EXPL_DLT) * self.epsilon[s]
            #print(self.epsilon[s], s)
            return a

        return 0


    def _agent_ctl_action(self):
        #print('action sel step')
        #self.last_action_lock.acquire()
        self._agent_action(self.curr_radio.get_radio_id(), self.last_action)
        #self.last_action_lock.release()

    # set alarm for periodic step interrupt
    def set_step_interrupt(self, step_time, action_time):
        self.step_int = Timer.Alarm(self._agent_ctl_step, step_time, periodic=True)
        #self.action_int = Timer.Alarm(self._agent_ctl_action, action_time, periodic=True)

    # interrupt the waiting time step and force agent to step the environmnet
    def agent_interrupt_step(self):
        pass

    def _agent_action(self, curr_radio_id, a):
        if isinstance(self.action_space, RLActionSpace_Radio):
            if a == self.action_space.ACTION_RADIO_0:
                self.action_select_cb(curr_radio_id, False)
            elif a == self.action_space.ACTION_RADIO_1:
                self.action_select_cb(curr_radio_id, False)
            elif a == self.action_space.ACTION_RADIO_OFF:
                self.action_select_cb(curr_radio_id, True)
        elif isinstance(self.action_space, RLActionSpace_RadioPwr):
            #print('agent action', curr_radio_id, a)
            #if curr_radio_id == 0:
            if a == self.action_space.RADIO_0_P_0:
                self.action_select_cb(0, False, 0)
            elif a == self.action_space.RADIO_0_P_1:
                self.action_select_cb(0, False, 1)
            elif a == self.action_space.RADIO_0_P_2:
                self.action_select_cb(0, False, 2)
            elif a == self.action_space.RADIO_0_P_3:
                self.action_select_cb(0, False, 3)
            elif a == self.action_space.RADIO_0_P_4:
                self.action_select_cb(0, False, 4)
            #elif curr_radio_id == 1:
            elif a == self.action_space.RADIO_1_P_0:
                self.action_select_cb(1, False, 0)
            elif a == self.action_space.RADIO_1_P_1:
                self.action_select_cb(1, False, 1)
            elif a == self.action_space.RADIO_1_P_2:
                self.action_select_cb(1, False, 2)
            elif a == self.action_space.RADIO_1_P_3:
                self.action_select_cb(1, False, 3)
            elif a == self.action_space.RADIO_1_P_4:
                self.action_select_cb(1, False, 4)
        
        self.log_cb()


    def reset_mem(self):
        self.q_table = [np.zeros((self.state_space.state_space_size(), self.action_space.action_space_size())) for i in range(self.n_components)]
        self.n_steps = 0

    def set_w1(self, w1):
        self.obj_weights = np.array([w1, 1-w1])